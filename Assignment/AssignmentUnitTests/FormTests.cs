﻿//-------------------------------------------------------------------------------------------------
// Author     : Tom Chambers
// Created    : 26/04/2015
// Description: Unit tests for methods in the Form1 class.
// Version    : 1.0
//-------------------------------------------------------------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment;

namespace AssignmentUnitTests
{
    /// <summary>
    /// A unit test class for testing methods in the Form1 class.
    /// </summary>
    [TestClass]
    public class FormTests
    {
        /// <summary>
        /// Checks that the method CheckSelectedArea returns false when the start value is greater than the end value.
        /// </summary>
        [TestCategory("GraphSelection")]
        [TestMethod]
        public void CheckSelectedAreaReturnsFalseWhenAreaStartIsGreaterThanAreaEnd()
        {
            double areaStart = 1000;
            double areaEnd = 750;

            Form1 testForm = new Form1();

            bool validSelection = testForm.CheckSelectedArea(areaStart, areaEnd);

            Assert.IsFalse(validSelection);
        }

        /// <summary>
        /// Checks that the method CheckSelectedArea returns false when the start value is less than zero.
        /// </summary>
        [TestCategory("GraphSelection")]
        [TestMethod]
        public void CheckSelectedAreaReturnsFalseWhenAreaStartIsLessThanZero()
        {
            double areaStart = -250;
            double areaEnd = 1600;

            Form1 testForm = new Form1();

            bool validSelection = testForm.CheckSelectedArea(areaStart, areaEnd);

            Assert.IsFalse(validSelection);
        }
    }
}

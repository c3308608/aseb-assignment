﻿//-------------------------------------------------------------------------------------------------
// Author     : Tom Chambers
// Created    : 26/02/2015
// Description: Unit tests for methods in the Session class.
// Version    : 1.0
//-------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment;

namespace AssignmentUnitTests
{
    /// <summary>
    /// A unit test class for testing methods in the Session class.
    /// </summary>
    [TestClass]
    public class SessionTests
    {
        /*
         * A list of test speed values.
         */
        List<double> speeds = new List<double>
        { 
            20.2, 22.3, 23.5, 25.4, 28.0, 30.7, 34.2, 31.1, 24.6, 19.8  
        };

        /*
         * A list of test heart rate values.
         */
        List<int> heartRates = new List<int>
        { 
            110, 114, 118, 122, 126, 130, 134, 138, 142, 147, 145, 143, 142, 150, 147  
        };

        /*
         * A list of test power values.
         */
        List<int> powerData = new List<int>
        {
            244, 255, 276, 281, 294, 303, 330, 345, 332, 329, 343, 325, 318, 321, 328
        };

        /// <summary>
        /// Checks that the method TotalDistance returns the correct value when it is passed a double representing speed
        /// and a TimeSpan object representing duration.
        /// </summary>
        [TestCategory("Distance")]
        [TestMethod]
        public void TotalDistanceReturnsCorrectDistance()
        {
            Session testSession = new Session();

            double speed = 23.4;

            TimeSpan time = new TimeSpan(1, 4, 7);

            double distance = testSession.TotalDistance(speed, time);

            Assert.AreEqual(25.01, distance);
        }

        /// <summary>
        /// Checks that the method ConvertMetricToImperial returns the correct imperial value when it is passed a 
        /// double representing a metric value.
        /// </summary>
        [TestCategory("Conversion")]
        [TestMethod]
        public void ConvertMetricToImperialReturnsCorrectValue()
        {
            Session testSession = new Session();

            double metricValue = 48.2;

            double imperialConversion = testSession.ConvertMetricToImperial(metricValue);

            Assert.AreEqual(29.95, imperialConversion);
        }

        /// <summary>
        /// Checks that the method ConvertImperialToMetric returns the correct metric value when it is passed a 
        /// double representing an imperial value.
        /// </summary>
        [TestCategory("Conversion")]
        [TestMethod]
        public void ConvertImperialToMetricReturnsCorrectValue()
        {
            Session testSession = new Session();

            double imperialValue = 34.5;

            double metricConversion = testSession.ConvertImperialToMetric(imperialValue);

            Assert.AreEqual(55.52, metricConversion);
        }

        /// <summary>
        /// Checks that the method GetHrmFileData returns false when it is given an invalid file path.
        /// </summary>
        [TestCategory("File")]
        [TestMethod]
        public void GetHrmFileDataReturnsFalseWhenFilePathIsInvalid()
        {
            Session testSession = new Session();

            string invalidFilePath = "C:\\This path leads to nowhere";

            bool result = testSession.GetHrmFileData(invalidFilePath);

            Assert.IsFalse(result);
        }

        /// <summary>
        /// Checks that the method CalculateAverageSpeed returns the correct average when it is passed a
        /// list of doubles representing speeds.
        /// </summary>
        [TestCategory("Summary Data")]
        [TestMethod]
        public void CalculateAverageSpeedReturnsCorrectValue()
        {
            Session testSession = new Session();

            double averageSpeed = testSession.CalculateAverageSpeed(speeds);

            Assert.AreEqual(25.98, averageSpeed);
        }

        /// <summary>
        /// Checks that the method MaximumSpeed returns the highest value when it is passed a list of
        /// doubles representing speeds.
        /// </summary>
        [TestCategory("Summary Data")]
        [TestMethod]
        public void MaxiumumSpeedReturnsCorrectValue()
        {
            Session testSession = new Session();

            double maximumSpeed = testSession.MaximumSpeed(speeds);

            Assert.AreEqual(34.2, maximumSpeed);
        }

        /// <summary>
        /// Checks that the method CalculateAverageHeartRate returns the correct average when it is passed a
        /// list of integers representing heart beats per minute.
        /// </summary>
        [TestCategory("Summary Data")]
        [TestMethod]
        public void CalculateAverageHeartRateReturnsCorrectValue()
        {
            Session testSession = new Session();

            int averageHeartRate = (int)testSession.CalculateAverageHeartRate(heartRates);

            Assert.AreEqual(134, averageHeartRate);
        }

        /// <summary>
        /// Checks that the method MaximumHeartRate returns the highest value when it is passed a list of
        /// integers representing heart beats per minute.
        /// </summary>
        [TestCategory("Summary Data")]
        [TestMethod]
        public void MaxiumumHeartRateReturnsCorrectValue()
        {
            Session testSession = new Session();

            int maximumHeartRate = testSession.MaximumHeartRate(heartRates);

            Assert.AreEqual(150, maximumHeartRate);
        }

        /// <summary>
        /// Checks that the method MinimumHeartRate returns the lowest value when it is passed a list of
        /// integers representing heart beats per minute.
        /// </summary>
        [TestCategory("Summary Data")]
        [TestMethod]
        public void MiniumumHeartRateReturnsCorrectValue()
        {
            Session testSession = new Session();

            int minimumHeartRate = testSession.MinimumHeartRate(heartRates);

            Assert.AreEqual(110, minimumHeartRate);
        }

        /// <summary>
        /// Checks that the method CalculateAveragePower returns the correct average when it is passed a
        /// list of integers representing power in watts.
        /// </summary>
        [TestCategory("Summary Data")]
        [TestMethod]
        public void CalculateAveragePowerReturnsCorrectValue()
        {
            Session testSession = new Session();

            int averagePower = (int)testSession.CalculateAveragePower(powerData);

            Assert.AreEqual(308, averagePower);
        }

        /// <summary>
        /// Checks that the method MaximumPower returns the highest value when it is passed a list of
        /// integers representing power in watts.
        /// </summary>
        [TestCategory("Summary Data")]
        [TestMethod]
        public void MaxiumumPowerReturnsCorrectValue()
        {
            Session testSession = new Session();

            int maximumPower = testSession.MaximumPower(powerData);

            Assert.AreEqual(345, maximumPower);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using ZedGraph;

namespace Assignment
{
    public partial class Form1 : Form
    {
        private string path;
        private string filename;

        private Cursor waiting = Cursors.WaitCursor;

        private Session session;
        private Coggan coggan;

        private GraphPane graph;

        ///*
        // * Attributes for selectable graph information.
        // */
        private double xStart;
        private double yStart;
        private double y2Start;
        private double xEnd;
        private double yEnd;
        private double y2End;
        private double xMove;
        private double yMove;
        private double y2Move;
        private bool selectedData;

        private bool convertedToImperial;
        private bool convertedToMetric;

        private List<int> heartData;
        private List<double> speedData;
        private List<int> cadenceData;
        private List<int> altitudeData;
        private List<int> powerData;

        public Form1()
        {
            InitializeComponent();
            zedGraphControl1.GraphPane.Title = "";
            zedGraphControl1.GraphPane.XAxis.Title = "";
            zedGraphControl1.GraphPane.YAxis.Title = "";
        }

        private void DisplayHeaderData()
        {
            textBoxSMode.Text = session.sMode;
            textBoxSessionDate.Text = session.sessionDate.Date.ToString("d");
            textBoxStartTime.Text = session.startTime.ToString();
            textBoxSessionLength.Text = session.sessionLength.ToString();
            textBoxInterval.Text = session.interval.ToString();
            textBoxMaxHR.Text = session.maxHR.ToString();
            textBoxRestHR.Text = session.restHR.ToString();
            textBoxStartDelay.Text = session.startDelay.ToString();
            textBoxVO2max.Text = session.vO2max.ToString();
            textBoxWeight.Text = session.weight.ToString();
        }

        private void DisplayColumnData()
        {
            for (int row = 0; row < session.columnRows.Count(); row++)
            {
                dataGridView1.Rows.Add(row + 1, session.heart.ElementAt(row), session.speed.ElementAt(row), session.cadence.ElementAt(row),
                                       session.altitude.ElementAt(row), session.power.ElementAt(row), session.powerBalance.ElementAt(row),
                                       session.dataTimes.ElementAt(row), session.sessionDate.Date.ToString("d"));
            }
        }

        private void DisplaySummaryData(List<double> speedList, List<int> heartList, List<int> powerList, List<int> altitudeList)
        {
            /*
             * The total time in seconds of the data for which to calculate summary data.
             */
            TimeSpan dataTime;

            if (selectedData == true)
            {
                dataTime = new TimeSpan(0, 0, speedList.Count() * session.interval);
            }
            else
            {
                dataTime = session.sessionLength;
            }

            textBoxTotalDistance.Text = session.TotalDistance(session.CalculateAverageSpeed(speedList), dataTime).ToString();
            textBoxAverageSpeed.Text = session.CalculateAverageSpeed(speedList).ToString();
            textBoxMaximumSpeed.Text = session.MaximumSpeed(speedList).ToString();
            textBoxAverageHeartRate.Text = session.CalculateAverageHeartRate(heartList).ToString();
            textBoxMaximumHeartRate.Text = session.MaximumHeartRate(heartList).ToString();
            textBoxMinimumHeartRate.Text = session.MinimumHeartRate(heartList).ToString();
            textBoxAveragePower.Text = session.CalculateAveragePower(powerList).ToString();
            textBoxMaximumPower.Text = session.MaximumPower(powerList).ToString();
            textBoxAverageAltitude.Text = session.CalculateAverageAltitude(altitudeList).ToString();
            textBoxMaximumAltitude.Text = session.MaximumAltitude(altitudeList).ToString();
        }

        private void ClearData()
        {
            dataGridView1.Rows.Clear();
            textBoxTotalDistance.Text = "";
            textBoxAverageSpeed.Text = "";
            textBoxMaximumSpeed.Text = "";
            textBoxAverageHeartRate.Text = "";
            textBoxMaximumHeartRate.Text = "";
            textBoxMinimumHeartRate.Text = "";
            textBoxAveragePower.Text = "";
            textBoxMaximumPower.Text = "";
            textBoxAverageAltitude.Text = "";
            textBoxMaximumAltitude.Text = "";
        }

        private void buttonShowData_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "HRM | *.hrm";
            openFileDialog1.FileName = "";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.Cursor = waiting;

                path = openFileDialog1.FileName;
                filename = Path.GetFileName(openFileDialog1.FileName);

                session = new Session();

                if (session.GetHrmFileData(path))
                {
                    ClearData();

                    DisplayHeaderData();
                    DisplayColumnData();

                    labelFileName.Text = "Showing data from file: " + filename;

                    heartData = session.heart;
                    speedData = session.speed;
                    cadenceData = session.cadence;
                    altitudeData = session.altitude;
                    powerData = session.power;

                    if (session.sModeUnits == 0)
                    {
                        labelSummaryDataDisplay.Text = "Displaying summmary data in metric";
                    }
                    else
                    {
                        labelSummaryDataDisplay.Text = "Displaying summmary data in imperial";
                    }

                    coggan = new Coggan();

                    double normalisedPower = coggan.NormalisedPower(session.power);

                    textBoxNP.Text = normalisedPower.ToString();

                    if (!string.IsNullOrWhiteSpace(textBoxFTP.Text))
                    {
                        double intensityFactor = coggan.IntensityFactor(Double.Parse(textBoxFTP.Text), normalisedPower);

                        textBoxIF.Text = intensityFactor.ToString();
                        textBoxTSS.Text = coggan.TrainingStressScore(session.power, normalisedPower, intensityFactor, Double.Parse(textBoxFTP.Text)).ToString();
                    }

                    DrawGraph(true, true, true, true, true, null, null);
                }
                else
                {
                    session = null;
                    MessageBox.Show(Session.errorMessage);
                }

                this.Cursor = null;
            }
        }

        private void buttonMetric_Click(object sender, EventArgs e)
        {
            if (session != null && selectedData != true)
            {
                if (session.sModeUnits == 0) // 0 = metric (kilometres/kph)
                {
                    DisplaySummaryData(session.speed, session.heart, session.power, session.altitude);

                    labelSummaryDataDisplay.Text = "Displaying summmary data in metric";

                    if (convertedToImperial == true)
                    {
                        speedData = session.ConvertImperialSpeedsToMetric(speedData);

                        DrawGraph(checkBoxHeartRate.Checked, checkBoxSpeed.Checked, checkBoxCadence.Checked,
                                  checkBoxAltitude.Checked, checkBoxPower.Checked, null, null);

                        convertedToImperial = false;
                    }
                }
                else
                {
                    heartData = session.heart;
                    speedData = session.ConvertImperialSpeedsToMetric(session.speed);
                    cadenceData = session.cadence;
                    altitudeData = session.altitude;
                    powerData = session.power;

                    DisplaySummaryData(speedData, heartData, powerData, altitudeData);

                    DrawGraph(checkBoxHeartRate.Checked, checkBoxSpeed.Checked, checkBoxCadence.Checked,
                              checkBoxAltitude.Checked, checkBoxPower.Checked, null, null);

                    convertedToMetric = true;

                    labelSummaryDataDisplay.Text = "Displaying summmary data in metric";
                }
            }
        }

        private void buttonImperial_Click(object sender, EventArgs e)
        {
            if (session != null && selectedData != true)
            {
                if (session.sModeUnits == 1) // 1 = imperial (miles/mph)
                {
                    DisplaySummaryData(session.speed, session.heart, session.power, session.altitude);

                    labelSummaryDataDisplay.Text = "Displaying summmary data in imperial";

                    if (convertedToMetric == true)
                    {
                        speedData = session.ConvertMetricSpeedsToImperial(speedData);

                        DrawGraph(checkBoxHeartRate.Checked, checkBoxSpeed.Checked, checkBoxCadence.Checked,
                                  checkBoxAltitude.Checked, checkBoxPower.Checked, null, null);

                        convertedToMetric = false;
                    }
                }
                else
                {
                    heartData = session.heart;
                    speedData = session.ConvertMetricSpeedsToImperial(session.speed);
                    cadenceData = session.cadence;
                    altitudeData = session.altitude;
                    powerData = session.power;

                    DisplaySummaryData(speedData, heartData, powerData, altitudeData);

                    DrawGraph(checkBoxHeartRate.Checked, checkBoxSpeed.Checked, checkBoxCadence.Checked,
                              checkBoxAltitude.Checked, checkBoxPower.Checked, null, null);

                    convertedToImperial = true;

                    labelSummaryDataDisplay.Text = "Displaying summmary data in imperial";
                }
            }
        }

        private void DrawGraph(bool heart, bool speed, bool cadence, bool altitude, bool power, double? xStart, double? xEnd)
        {
            ClearGraph();

            graph = zedGraphControl1.GraphPane;

            graph.XAxis.Min = session.interval;
            graph.XAxis.Max = session.intervals;

            List<int> selectedHeartData = new List<int>();
            List<double> selectedSpeedData = new List<double>();
            List<int> selectedCadenceData = new List<int>();
            List<int> selectedAltitudeData = new List<int>();
            List<int> selectedPowerData = new List<int>();

            if (xStart == null || xEnd == null || xStart == xEnd || selectedData == true)
            {
                selectedHeartData = heartData;
                selectedSpeedData = speedData;
                selectedCadenceData = cadenceData;
                selectedAltitudeData = altitudeData;
                selectedPowerData = powerData;

                textBoxMouseDown.Text = "";
                textBoxMouseUp.Text = "";

                selectedData = false;
            }
            else
            {
                selectedHeartData.AddRange(heartData.GetRange((int)xStart, (int)xEnd - (int)xStart));
                selectedSpeedData.AddRange(speedData.GetRange((int)xStart, (int)xEnd - (int)xStart));
                selectedCadenceData.AddRange(cadenceData.GetRange((int)xStart, (int)xEnd - (int)xStart));
                selectedAltitudeData.AddRange(altitudeData.GetRange((int)xStart, (int)xEnd - (int)xStart));
                selectedPowerData.AddRange(powerData.GetRange((int)xStart, (int)xEnd - (int)xStart));
                graph.XAxis.Max = selectedHeartData.Count;
                selectedData = true;
            }

            PointPairList heartPointPairList = new PointPairList();
            PointPairList speedPointPairList = new PointPairList();
            PointPairList cadencePointPairList = new PointPairList();
            PointPairList altitudePointPairList = new PointPairList();
            PointPairList powerPointPairList = new PointPairList();

            if (heart == true)
            {
                for (int i = 0; i < selectedHeartData.Count(); i++)
                {
                    heartPointPairList.Add(session.interval + (i * session.interval), selectedHeartData.ElementAt(i));
                }

                LineItem heartLine = graph.AddCurve("Heart", heartPointPairList, Color.Red, SymbolType.None);
                heartLine.Line.Width = 2;
            }

            if (speed == true)
            {
                for (int i = 0; i < selectedSpeedData.Count(); i++)
                {
                    speedPointPairList.Add(session.interval + (i * session.interval), selectedSpeedData.ElementAt(i));
                }

                LineItem speedLine = graph.AddCurve("Speed", speedPointPairList, Color.Blue, SymbolType.None);
                speedLine.Line.Width = 2;
            }

            if (cadence == true)
            {
                for (int i = 0; i < selectedCadenceData.Count(); i++)
                {
                    cadencePointPairList.Add(session.interval + (i * session.interval), selectedCadenceData.ElementAt(i));
                }

                LineItem cadenceLine = graph.AddCurve("Cadence", cadencePointPairList, Color.Yellow, SymbolType.None);
                cadenceLine.Line.Width = 2;
            }

            if (altitude == true)
            {
                for (int i = 0; i < selectedAltitudeData.Count(); i++)
                {
                    altitudePointPairList.Add(session.interval + (i * session.interval), selectedAltitudeData.ElementAt(i));
                }

                LineItem altitudeLine = graph.AddCurve("Altitude", altitudePointPairList, Color.Green, SymbolType.None);
                altitudeLine.Line.Width = 2;
            }

            if (power == true)
            {
                for (int i = 0; i < selectedPowerData.Count(); i++)
                {
                    powerPointPairList.Add(session.interval + (i * session.interval), selectedPowerData.ElementAt(i));
                }

                LineItem powerLine = graph.AddCurve("Power", powerPointPairList, Color.Black, SymbolType.None);
                powerLine.Line.Width = 2;
            }

            zedGraphControl1.AxisChange();
            zedGraphControl1.Refresh();

            DisplaySummaryData(selectedSpeedData, selectedHeartData, selectedPowerData, selectedAltitudeData);

            double normalisedPower = coggan.NormalisedPower(selectedPowerData);

            textBoxNP.Text = normalisedPower.ToString();

            if (!string.IsNullOrWhiteSpace(textBoxFTP.Text))
            {
                double intensityFactor = coggan.IntensityFactor(Double.Parse(textBoxFTP.Text), normalisedPower);

                textBoxIF.Text = intensityFactor.ToString();
                textBoxTSS.Text = coggan.TrainingStressScore(selectedPowerData, normalisedPower, intensityFactor, Double.Parse(textBoxFTP.Text)).ToString();
            }
        }

        private void ClearGraph()
        {
            zedGraphControl1.GraphPane.CurveList.Clear();
        }

        private void buttonDisplayGraph_Click(object sender, EventArgs e)
        {
            if (session != null)
            {
                if (selectedData == true)
                {
                    selectedData = false;

                    DrawGraph(checkBoxHeartRate.Checked, checkBoxSpeed.Checked, checkBoxCadence.Checked,
                          checkBoxAltitude.Checked, checkBoxPower.Checked, xStart, xEnd);
                }
                else
                {
                    DrawGraph(checkBoxHeartRate.Checked, checkBoxSpeed.Checked, checkBoxCadence.Checked,
                              checkBoxAltitude.Checked, checkBoxPower.Checked, null, null);
                }
            }
        }

        private void zedGraphControl1_MouseDown(object sender, MouseEventArgs e)
        {
            if (graph != null)
            {
                PointPairList selectionStartPoint = new PointPairList();
                LineItem selectionStartLine = new LineItem("SelectionStartLine");

                Point mousePoint = new Point(e.X, e.Y);

                graph.ReverseTransform(mousePoint, out xStart, out yStart, out y2Start);

                xStart = Math.Round(xStart, 0);

                selectionStartPoint.Add(xStart, graph.YAxis.Min);
                selectionStartPoint.Add(xStart, graph.YAxis.Max);

                selectionStartLine = graph.AddCurve(null, selectionStartPoint, Color.DarkOrange, SymbolType.None);
                selectionStartLine.Line.Width = 2;

                graph.CurveList.Move(graph.CurveList.Count - 1, -100);

                zedGraphControl1.Refresh();

                textBoxMouseDown.Text = xStart.ToString();
            }
        }

        private void zedGraphControl1_MouseMove(object sender, MouseEventArgs e)
        {
            if (graph != null)
            {
                if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
                {
                    PointPairList selectionEndPoint = new PointPairList();
                    LineItem selectionEndLine = new LineItem("SelectionStartLine");

                    Point mousePoint = new Point(e.X, e.Y);

                    graph.ReverseTransform(mousePoint, out xMove, out yMove, out y2Move);

                    xMove = Math.Round(xMove, 0);

                    selectionEndPoint.Clear();

                    selectionEndPoint.Add(xMove, graph.YAxis.Min);
                    selectionEndPoint.Add(xMove, graph.YAxis.Max);

                    selectionEndLine = graph.AddCurve(null, selectionEndPoint, Color.DarkOrange, SymbolType.None);
                    selectionEndLine.Line.Width = 2;

                    graph.CurveList.Move(graph.CurveList.Count - 1, -100);

                    zedGraphControl1.Refresh();

                    textBoxMouseUp.Text = xMove.ToString();

                    selectionEndLine.Clear();
                }
            }
        }

        private void zedGraphControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (graph != null)
            {
                Point mousePoint = new Point(e.X, e.Y);

                graph.ReverseTransform(mousePoint, out xEnd, out yEnd, out y2End);

                xEnd = Math.Round(xEnd, 0);

                textBoxMouseUp.Text = xEnd.ToString();

                if (CheckSelectedArea(xStart, xEnd))
                {
                    DrawGraph(checkBoxHeartRate.Checked, checkBoxSpeed.Checked, checkBoxCadence.Checked,
                              checkBoxAltitude.Checked, checkBoxPower.Checked, xStart, xEnd);
                }
            }
        }

        public bool CheckSelectedArea(double areaStart, double areaEnd)
        {
            if (areaStart > areaEnd)
            {
                return false;
            }
            else if (areaStart < 0)
            {
                return false;
            }
            else if (areaEnd > graph.XAxis.Max)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}

﻿//-------------------------------------------------------------------------------------------------
// Author     : Tom Chambers
// Created    : 06/05/2015
// Description: Functionality for calculating additional metrics.
// Version    : 1.0
//-------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    /// <summary>
    /// This class provides functionality for calculating the additional metrics normalised power,
    /// intesity factor, and training stress score.
    /// </summary>
    public class Coggan
    {
        public double NormalisedPower(List<int> powerList)
        {
            List<double> rollingAverageToPowerOfFour = new List<double>();

            if (powerList.Count > 29)
            {
                for (int i = 29; i < powerList.Count; i++)
                {
                    double rollingAverage = Math.Round(powerList.GetRange(i - 29, 30).Average());

                    double toPowerOfFour = Math.Pow(rollingAverage, 4);

                    rollingAverageToPowerOfFour.Add(toPowerOfFour);
                }

                double normalisedPower = Math.Pow(rollingAverageToPowerOfFour.Average(), 0.25);

                return normalisedPower;
            }

            return 0;
        }

        public double IntensityFactor(double ftp, double normalisedPower)
        {
            return normalisedPower / ftp;
        }

        public double TrainingStressScore(List<int> powerList, double normalisedPower, double intensityFactor, double ftp)
        {
            double totalWork = normalisedPower * powerList.Count;

            double rawTSS = totalWork * intensityFactor;

            return rawTSS / (ftp * 3600) * 100;
        }
    }
}

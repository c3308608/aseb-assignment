﻿namespace Assignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelFTP = new System.Windows.Forms.Label();
            this.labelIF = new System.Windows.Forms.Label();
            this.labelNP = new System.Windows.Forms.Label();
            this.labelTSS = new System.Windows.Forms.Label();
            this.textBoxIF = new System.Windows.Forms.TextBox();
            this.textBoxNP = new System.Windows.Forms.TextBox();
            this.textBoxTSS = new System.Windows.Forms.TextBox();
            this.textBoxFTP = new System.Windows.Forms.TextBox();
            this.labelSelectionEnd = new System.Windows.Forms.Label();
            this.labelSelectionStart = new System.Windows.Forms.Label();
            this.textBoxMouseUp = new System.Windows.Forms.TextBox();
            this.textBoxMouseDown = new System.Windows.Forms.TextBox();
            this.buttonDisplayGraph = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxPower = new System.Windows.Forms.CheckBox();
            this.checkBoxAltitude = new System.Windows.Forms.CheckBox();
            this.checkBoxCadence = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed = new System.Windows.Forms.CheckBox();
            this.checkBoxHeartRate = new System.Windows.Forms.CheckBox();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.labelSummaryDataDisplay = new System.Windows.Forms.Label();
            this.buttonMetric = new System.Windows.Forms.Button();
            this.buttonImperial = new System.Windows.Forms.Button();
            this.labelSummaryDataValues = new System.Windows.Forms.Label();
            this.labelTotalDistance = new System.Windows.Forms.Label();
            this.textBoxTotalDistance = new System.Windows.Forms.TextBox();
            this.labelMaximumSpeed = new System.Windows.Forms.Label();
            this.textBoxMaximumSpeed = new System.Windows.Forms.TextBox();
            this.textBoxMaximumAltitude = new System.Windows.Forms.TextBox();
            this.labelMaximumAltitude = new System.Windows.Forms.Label();
            this.textBoxAverageAltitude = new System.Windows.Forms.TextBox();
            this.labelAverageAltitude = new System.Windows.Forms.Label();
            this.labelMaximumPower = new System.Windows.Forms.Label();
            this.textBoxMaximumPower = new System.Windows.Forms.TextBox();
            this.labelAveragePower = new System.Windows.Forms.Label();
            this.textBoxAveragePower = new System.Windows.Forms.TextBox();
            this.textBoxMinimumHeartRate = new System.Windows.Forms.TextBox();
            this.labelMinimumHeartRate = new System.Windows.Forms.Label();
            this.textBoxMaximumHeartRate = new System.Windows.Forms.TextBox();
            this.labelMaximumHeartRate = new System.Windows.Forms.Label();
            this.labelAverageHeartRate = new System.Windows.Forms.Label();
            this.textBoxAverageHeartRate = new System.Windows.Forms.TextBox();
            this.textBoxAverageSpeed = new System.Windows.Forms.TextBox();
            this.labelAverageSpeed = new System.Windows.Forms.Label();
            this.Weight = new System.Windows.Forms.Label();
            this.labelVO2max = new System.Windows.Forms.Label();
            this.textBoxWeight = new System.Windows.Forms.TextBox();
            this.textBoxVO2max = new System.Windows.Forms.TextBox();
            this.textBoxStartDelay = new System.Windows.Forms.TextBox();
            this.labelStartDelay = new System.Windows.Forms.Label();
            this.labelRestHR = new System.Windows.Forms.Label();
            this.textBoxRestHR = new System.Windows.Forms.TextBox();
            this.textBoxMaxHR = new System.Windows.Forms.TextBox();
            this.labelMaxHR = new System.Windows.Forms.Label();
            this.labelSMode = new System.Windows.Forms.Label();
            this.textBoxSMode = new System.Windows.Forms.TextBox();
            this.textBoxSessionLength = new System.Windows.Forms.TextBox();
            this.labelSessionLength = new System.Windows.Forms.Label();
            this.labelSessionDate = new System.Windows.Forms.Label();
            this.textBoxSessionDate = new System.Windows.Forms.TextBox();
            this.labelFileName = new System.Windows.Forms.Label();
            this.buttonShowData = new System.Windows.Forms.Button();
            this.labelInterval = new System.Windows.Forms.Label();
            this.textBoxInterval = new System.Windows.Forms.TextBox();
            this.labelStartTime = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxStartTime = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelFTP);
            this.panel1.Controls.Add(this.labelIF);
            this.panel1.Controls.Add(this.labelNP);
            this.panel1.Controls.Add(this.labelTSS);
            this.panel1.Controls.Add(this.textBoxIF);
            this.panel1.Controls.Add(this.textBoxNP);
            this.panel1.Controls.Add(this.textBoxTSS);
            this.panel1.Controls.Add(this.textBoxFTP);
            this.panel1.Controls.Add(this.labelSelectionEnd);
            this.panel1.Controls.Add(this.labelSelectionStart);
            this.panel1.Controls.Add(this.textBoxMouseUp);
            this.panel1.Controls.Add(this.textBoxMouseDown);
            this.panel1.Controls.Add(this.buttonDisplayGraph);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.zedGraphControl1);
            this.panel1.Controls.Add(this.labelSummaryDataDisplay);
            this.panel1.Controls.Add(this.buttonMetric);
            this.panel1.Controls.Add(this.buttonImperial);
            this.panel1.Controls.Add(this.labelSummaryDataValues);
            this.panel1.Controls.Add(this.labelTotalDistance);
            this.panel1.Controls.Add(this.textBoxTotalDistance);
            this.panel1.Controls.Add(this.labelMaximumSpeed);
            this.panel1.Controls.Add(this.textBoxMaximumSpeed);
            this.panel1.Controls.Add(this.textBoxMaximumAltitude);
            this.panel1.Controls.Add(this.labelMaximumAltitude);
            this.panel1.Controls.Add(this.textBoxAverageAltitude);
            this.panel1.Controls.Add(this.labelAverageAltitude);
            this.panel1.Controls.Add(this.labelMaximumPower);
            this.panel1.Controls.Add(this.textBoxMaximumPower);
            this.panel1.Controls.Add(this.labelAveragePower);
            this.panel1.Controls.Add(this.textBoxAveragePower);
            this.panel1.Controls.Add(this.textBoxMinimumHeartRate);
            this.panel1.Controls.Add(this.labelMinimumHeartRate);
            this.panel1.Controls.Add(this.textBoxMaximumHeartRate);
            this.panel1.Controls.Add(this.labelMaximumHeartRate);
            this.panel1.Controls.Add(this.labelAverageHeartRate);
            this.panel1.Controls.Add(this.textBoxAverageHeartRate);
            this.panel1.Controls.Add(this.textBoxAverageSpeed);
            this.panel1.Controls.Add(this.labelAverageSpeed);
            this.panel1.Controls.Add(this.Weight);
            this.panel1.Controls.Add(this.labelVO2max);
            this.panel1.Controls.Add(this.textBoxWeight);
            this.panel1.Controls.Add(this.textBoxVO2max);
            this.panel1.Controls.Add(this.textBoxStartDelay);
            this.panel1.Controls.Add(this.labelStartDelay);
            this.panel1.Controls.Add(this.labelRestHR);
            this.panel1.Controls.Add(this.textBoxRestHR);
            this.panel1.Controls.Add(this.textBoxMaxHR);
            this.panel1.Controls.Add(this.labelMaxHR);
            this.panel1.Controls.Add(this.labelSMode);
            this.panel1.Controls.Add(this.textBoxSMode);
            this.panel1.Controls.Add(this.textBoxSessionLength);
            this.panel1.Controls.Add(this.labelSessionLength);
            this.panel1.Controls.Add(this.labelSessionDate);
            this.panel1.Controls.Add(this.textBoxSessionDate);
            this.panel1.Controls.Add(this.labelFileName);
            this.panel1.Controls.Add(this.buttonShowData);
            this.panel1.Controls.Add(this.labelInterval);
            this.panel1.Controls.Add(this.textBoxInterval);
            this.panel1.Controls.Add(this.labelStartTime);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.textBoxStartTime);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1359, 1000);
            this.panel1.TabIndex = 0;
            // 
            // labelFTP
            // 
            this.labelFTP.AutoSize = true;
            this.labelFTP.Location = new System.Drawing.Point(697, 296);
            this.labelFTP.Name = "labelFTP";
            this.labelFTP.Size = new System.Drawing.Size(81, 13);
            this.labelFTP.TabIndex = 127;
            this.labelFTP.Text = "Enter your FTP:";
            // 
            // labelIF
            // 
            this.labelIF.AutoSize = true;
            this.labelIF.Location = new System.Drawing.Point(697, 380);
            this.labelIF.Name = "labelIF";
            this.labelIF.Size = new System.Drawing.Size(79, 13);
            this.labelIF.TabIndex = 126;
            this.labelIF.Text = "Intensity Factor";
            // 
            // labelNP
            // 
            this.labelNP.AutoSize = true;
            this.labelNP.Location = new System.Drawing.Point(697, 341);
            this.labelNP.Name = "labelNP";
            this.labelNP.Size = new System.Drawing.Size(92, 13);
            this.labelNP.TabIndex = 125;
            this.labelNP.Text = "Normalised Power";
            // 
            // labelTSS
            // 
            this.labelTSS.AutoSize = true;
            this.labelTSS.Location = new System.Drawing.Point(697, 419);
            this.labelTSS.Name = "labelTSS";
            this.labelTSS.Size = new System.Drawing.Size(108, 13);
            this.labelTSS.TabIndex = 124;
            this.labelTSS.Text = "Training Stress Score";
            // 
            // textBoxIF
            // 
            this.textBoxIF.Location = new System.Drawing.Point(700, 396);
            this.textBoxIF.Name = "textBoxIF";
            this.textBoxIF.ReadOnly = true;
            this.textBoxIF.Size = new System.Drawing.Size(100, 20);
            this.textBoxIF.TabIndex = 123;
            // 
            // textBoxNP
            // 
            this.textBoxNP.Location = new System.Drawing.Point(700, 357);
            this.textBoxNP.Name = "textBoxNP";
            this.textBoxNP.ReadOnly = true;
            this.textBoxNP.Size = new System.Drawing.Size(100, 20);
            this.textBoxNP.TabIndex = 122;
            // 
            // textBoxTSS
            // 
            this.textBoxTSS.Location = new System.Drawing.Point(700, 435);
            this.textBoxTSS.Name = "textBoxTSS";
            this.textBoxTSS.ReadOnly = true;
            this.textBoxTSS.Size = new System.Drawing.Size(100, 20);
            this.textBoxTSS.TabIndex = 121;
            // 
            // textBoxFTP
            // 
            this.textBoxFTP.Location = new System.Drawing.Point(700, 311);
            this.textBoxFTP.Name = "textBoxFTP";
            this.textBoxFTP.Size = new System.Drawing.Size(100, 20);
            this.textBoxFTP.TabIndex = 120;
            // 
            // labelSelectionEnd
            // 
            this.labelSelectionEnd.AutoSize = true;
            this.labelSelectionEnd.Location = new System.Drawing.Point(910, 687);
            this.labelSelectionEnd.Name = "labelSelectionEnd";
            this.labelSelectionEnd.Size = new System.Drawing.Size(73, 13);
            this.labelSelectionEnd.TabIndex = 119;
            this.labelSelectionEnd.Text = "Selection End";
            // 
            // labelSelectionStart
            // 
            this.labelSelectionStart.AutoSize = true;
            this.labelSelectionStart.Location = new System.Drawing.Point(910, 647);
            this.labelSelectionStart.Name = "labelSelectionStart";
            this.labelSelectionStart.Size = new System.Drawing.Size(76, 13);
            this.labelSelectionStart.TabIndex = 118;
            this.labelSelectionStart.Text = "Selection Start";
            // 
            // textBoxMouseUp
            // 
            this.textBoxMouseUp.Location = new System.Drawing.Point(913, 703);
            this.textBoxMouseUp.Name = "textBoxMouseUp";
            this.textBoxMouseUp.Size = new System.Drawing.Size(100, 20);
            this.textBoxMouseUp.TabIndex = 117;
            // 
            // textBoxMouseDown
            // 
            this.textBoxMouseDown.Location = new System.Drawing.Point(913, 663);
            this.textBoxMouseDown.Name = "textBoxMouseDown";
            this.textBoxMouseDown.Size = new System.Drawing.Size(100, 20);
            this.textBoxMouseDown.TabIndex = 116;
            // 
            // buttonDisplayGraph
            // 
            this.buttonDisplayGraph.Location = new System.Drawing.Point(913, 611);
            this.buttonDisplayGraph.Name = "buttonDisplayGraph";
            this.buttonDisplayGraph.Size = new System.Drawing.Size(100, 23);
            this.buttonDisplayGraph.TabIndex = 115;
            this.buttonDisplayGraph.Text = "Update Display";
            this.buttonDisplayGraph.UseVisualStyleBackColor = true;
            this.buttonDisplayGraph.Click += new System.EventHandler(this.buttonDisplayGraph_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxPower);
            this.groupBox1.Controls.Add(this.checkBoxAltitude);
            this.groupBox1.Controls.Add(this.checkBoxCadence);
            this.groupBox1.Controls.Add(this.checkBoxSpeed);
            this.groupBox1.Controls.Add(this.checkBoxHeartRate);
            this.groupBox1.Location = new System.Drawing.Point(913, 461);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(100, 135);
            this.groupBox1.TabIndex = 114;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Display data";
            // 
            // checkBoxPower
            // 
            this.checkBoxPower.AutoSize = true;
            this.checkBoxPower.Checked = true;
            this.checkBoxPower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPower.Location = new System.Drawing.Point(7, 113);
            this.checkBoxPower.Name = "checkBoxPower";
            this.checkBoxPower.Size = new System.Drawing.Size(56, 17);
            this.checkBoxPower.TabIndex = 4;
            this.checkBoxPower.Text = "Power";
            this.checkBoxPower.UseVisualStyleBackColor = true;
            // 
            // checkBoxAltitude
            // 
            this.checkBoxAltitude.AutoSize = true;
            this.checkBoxAltitude.Checked = true;
            this.checkBoxAltitude.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAltitude.Location = new System.Drawing.Point(7, 90);
            this.checkBoxAltitude.Name = "checkBoxAltitude";
            this.checkBoxAltitude.Size = new System.Drawing.Size(61, 17);
            this.checkBoxAltitude.TabIndex = 3;
            this.checkBoxAltitude.Text = "Altitude";
            this.checkBoxAltitude.UseVisualStyleBackColor = true;
            // 
            // checkBoxCadence
            // 
            this.checkBoxCadence.AutoSize = true;
            this.checkBoxCadence.Checked = true;
            this.checkBoxCadence.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCadence.Location = new System.Drawing.Point(7, 67);
            this.checkBoxCadence.Name = "checkBoxCadence";
            this.checkBoxCadence.Size = new System.Drawing.Size(69, 17);
            this.checkBoxCadence.TabIndex = 2;
            this.checkBoxCadence.Text = "Cadence";
            this.checkBoxCadence.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed
            // 
            this.checkBoxSpeed.AutoSize = true;
            this.checkBoxSpeed.Checked = true;
            this.checkBoxSpeed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSpeed.Location = new System.Drawing.Point(7, 44);
            this.checkBoxSpeed.Name = "checkBoxSpeed";
            this.checkBoxSpeed.Size = new System.Drawing.Size(57, 17);
            this.checkBoxSpeed.TabIndex = 1;
            this.checkBoxSpeed.Text = "Speed";
            this.checkBoxSpeed.UseVisualStyleBackColor = true;
            // 
            // checkBoxHeartRate
            // 
            this.checkBoxHeartRate.AutoSize = true;
            this.checkBoxHeartRate.Checked = true;
            this.checkBoxHeartRate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHeartRate.Location = new System.Drawing.Point(7, 20);
            this.checkBoxHeartRate.Name = "checkBoxHeartRate";
            this.checkBoxHeartRate.Size = new System.Drawing.Size(78, 17);
            this.checkBoxHeartRate.TabIndex = 0;
            this.checkBoxHeartRate.Text = "Heart Rate";
            this.checkBoxHeartRate.UseVisualStyleBackColor = true;
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.IsShowPointValues = false;
            this.zedGraphControl1.Location = new System.Drawing.Point(13, 461);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.PointValueFormat = "G";
            this.zedGraphControl1.Size = new System.Drawing.Size(893, 500);
            this.zedGraphControl1.TabIndex = 113;
            this.zedGraphControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.zedGraphControl1_MouseDown);
            this.zedGraphControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.zedGraphControl1_MouseMove);
            this.zedGraphControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.zedGraphControl1_MouseUp);
            // 
            // labelSummaryDataDisplay
            // 
            this.labelSummaryDataDisplay.AutoSize = true;
            this.labelSummaryDataDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSummaryDataDisplay.Location = new System.Drawing.Point(10, 318);
            this.labelSummaryDataDisplay.Name = "labelSummaryDataDisplay";
            this.labelSummaryDataDisplay.Size = new System.Drawing.Size(0, 13);
            this.labelSummaryDataDisplay.TabIndex = 112;
            // 
            // buttonMetric
            // 
            this.buttonMetric.Location = new System.Drawing.Point(353, 291);
            this.buttonMetric.Name = "buttonMetric";
            this.buttonMetric.Size = new System.Drawing.Size(75, 23);
            this.buttonMetric.TabIndex = 111;
            this.buttonMetric.Text = "Metric";
            this.buttonMetric.UseVisualStyleBackColor = true;
            this.buttonMetric.Click += new System.EventHandler(this.buttonMetric_Click);
            // 
            // buttonImperial
            // 
            this.buttonImperial.Location = new System.Drawing.Point(469, 291);
            this.buttonImperial.Name = "buttonImperial";
            this.buttonImperial.Size = new System.Drawing.Size(75, 23);
            this.buttonImperial.TabIndex = 110;
            this.buttonImperial.Text = "Imperial";
            this.buttonImperial.UseVisualStyleBackColor = true;
            this.buttonImperial.Click += new System.EventHandler(this.buttonImperial_Click);
            // 
            // labelSummaryDataValues
            // 
            this.labelSummaryDataValues.AutoSize = true;
            this.labelSummaryDataValues.Location = new System.Drawing.Point(10, 296);
            this.labelSummaryDataValues.Name = "labelSummaryDataValues";
            this.labelSummaryDataValues.Size = new System.Drawing.Size(330, 13);
            this.labelSummaryDataValues.TabIndex = 109;
            this.labelSummaryDataValues.Text = "Show summary data in metric (kilometres/kph) or imperial (miles/mph)";
            // 
            // labelTotalDistance
            // 
            this.labelTotalDistance.AutoSize = true;
            this.labelTotalDistance.Location = new System.Drawing.Point(10, 341);
            this.labelTotalDistance.Name = "labelTotalDistance";
            this.labelTotalDistance.Size = new System.Drawing.Size(76, 13);
            this.labelTotalDistance.TabIndex = 108;
            this.labelTotalDistance.Text = "Total Distance";
            // 
            // textBoxTotalDistance
            // 
            this.textBoxTotalDistance.Location = new System.Drawing.Point(13, 357);
            this.textBoxTotalDistance.Name = "textBoxTotalDistance";
            this.textBoxTotalDistance.ReadOnly = true;
            this.textBoxTotalDistance.Size = new System.Drawing.Size(100, 20);
            this.textBoxTotalDistance.TabIndex = 107;
            // 
            // labelMaximumSpeed
            // 
            this.labelMaximumSpeed.AutoSize = true;
            this.labelMaximumSpeed.Location = new System.Drawing.Point(122, 380);
            this.labelMaximumSpeed.Name = "labelMaximumSpeed";
            this.labelMaximumSpeed.Size = new System.Drawing.Size(85, 13);
            this.labelMaximumSpeed.TabIndex = 106;
            this.labelMaximumSpeed.Text = "Maximum Speed";
            // 
            // textBoxMaximumSpeed
            // 
            this.textBoxMaximumSpeed.Location = new System.Drawing.Point(125, 396);
            this.textBoxMaximumSpeed.Name = "textBoxMaximumSpeed";
            this.textBoxMaximumSpeed.ReadOnly = true;
            this.textBoxMaximumSpeed.Size = new System.Drawing.Size(100, 20);
            this.textBoxMaximumSpeed.TabIndex = 105;
            // 
            // textBoxMaximumAltitude
            // 
            this.textBoxMaximumAltitude.Location = new System.Drawing.Point(469, 396);
            this.textBoxMaximumAltitude.Name = "textBoxMaximumAltitude";
            this.textBoxMaximumAltitude.ReadOnly = true;
            this.textBoxMaximumAltitude.Size = new System.Drawing.Size(100, 20);
            this.textBoxMaximumAltitude.TabIndex = 104;
            // 
            // labelMaximumAltitude
            // 
            this.labelMaximumAltitude.AutoSize = true;
            this.labelMaximumAltitude.Location = new System.Drawing.Point(466, 380);
            this.labelMaximumAltitude.Name = "labelMaximumAltitude";
            this.labelMaximumAltitude.Size = new System.Drawing.Size(89, 13);
            this.labelMaximumAltitude.TabIndex = 103;
            this.labelMaximumAltitude.Text = "Maximum Altitude";
            // 
            // textBoxAverageAltitude
            // 
            this.textBoxAverageAltitude.Location = new System.Drawing.Point(469, 357);
            this.textBoxAverageAltitude.Name = "textBoxAverageAltitude";
            this.textBoxAverageAltitude.ReadOnly = true;
            this.textBoxAverageAltitude.Size = new System.Drawing.Size(100, 20);
            this.textBoxAverageAltitude.TabIndex = 102;
            // 
            // labelAverageAltitude
            // 
            this.labelAverageAltitude.AutoSize = true;
            this.labelAverageAltitude.Location = new System.Drawing.Point(466, 341);
            this.labelAverageAltitude.Name = "labelAverageAltitude";
            this.labelAverageAltitude.Size = new System.Drawing.Size(85, 13);
            this.labelAverageAltitude.TabIndex = 101;
            this.labelAverageAltitude.Text = "Average Altitude";
            // 
            // labelMaximumPower
            // 
            this.labelMaximumPower.AutoSize = true;
            this.labelMaximumPower.Location = new System.Drawing.Point(350, 380);
            this.labelMaximumPower.Name = "labelMaximumPower";
            this.labelMaximumPower.Size = new System.Drawing.Size(84, 13);
            this.labelMaximumPower.TabIndex = 100;
            this.labelMaximumPower.Text = "Maximum Power";
            // 
            // textBoxMaximumPower
            // 
            this.textBoxMaximumPower.Location = new System.Drawing.Point(353, 396);
            this.textBoxMaximumPower.Name = "textBoxMaximumPower";
            this.textBoxMaximumPower.ReadOnly = true;
            this.textBoxMaximumPower.Size = new System.Drawing.Size(100, 20);
            this.textBoxMaximumPower.TabIndex = 99;
            // 
            // labelAveragePower
            // 
            this.labelAveragePower.AutoSize = true;
            this.labelAveragePower.Location = new System.Drawing.Point(350, 341);
            this.labelAveragePower.Name = "labelAveragePower";
            this.labelAveragePower.Size = new System.Drawing.Size(80, 13);
            this.labelAveragePower.TabIndex = 98;
            this.labelAveragePower.Text = "Average Power";
            // 
            // textBoxAveragePower
            // 
            this.textBoxAveragePower.Location = new System.Drawing.Point(353, 357);
            this.textBoxAveragePower.Name = "textBoxAveragePower";
            this.textBoxAveragePower.ReadOnly = true;
            this.textBoxAveragePower.Size = new System.Drawing.Size(100, 20);
            this.textBoxAveragePower.TabIndex = 97;
            // 
            // textBoxMinimumHeartRate
            // 
            this.textBoxMinimumHeartRate.Location = new System.Drawing.Point(237, 435);
            this.textBoxMinimumHeartRate.Name = "textBoxMinimumHeartRate";
            this.textBoxMinimumHeartRate.ReadOnly = true;
            this.textBoxMinimumHeartRate.Size = new System.Drawing.Size(100, 20);
            this.textBoxMinimumHeartRate.TabIndex = 96;
            // 
            // labelMinimumHeartRate
            // 
            this.labelMinimumHeartRate.AutoSize = true;
            this.labelMinimumHeartRate.Location = new System.Drawing.Point(234, 419);
            this.labelMinimumHeartRate.Name = "labelMinimumHeartRate";
            this.labelMinimumHeartRate.Size = new System.Drawing.Size(103, 13);
            this.labelMinimumHeartRate.TabIndex = 95;
            this.labelMinimumHeartRate.Text = "Minimum Heart Rate";
            // 
            // textBoxMaximumHeartRate
            // 
            this.textBoxMaximumHeartRate.Location = new System.Drawing.Point(237, 396);
            this.textBoxMaximumHeartRate.Name = "textBoxMaximumHeartRate";
            this.textBoxMaximumHeartRate.ReadOnly = true;
            this.textBoxMaximumHeartRate.Size = new System.Drawing.Size(100, 20);
            this.textBoxMaximumHeartRate.TabIndex = 94;
            // 
            // labelMaximumHeartRate
            // 
            this.labelMaximumHeartRate.AutoSize = true;
            this.labelMaximumHeartRate.Location = new System.Drawing.Point(234, 380);
            this.labelMaximumHeartRate.Name = "labelMaximumHeartRate";
            this.labelMaximumHeartRate.Size = new System.Drawing.Size(106, 13);
            this.labelMaximumHeartRate.TabIndex = 93;
            this.labelMaximumHeartRate.Text = "Maximum Heart Rate";
            // 
            // labelAverageHeartRate
            // 
            this.labelAverageHeartRate.AutoSize = true;
            this.labelAverageHeartRate.Location = new System.Drawing.Point(234, 341);
            this.labelAverageHeartRate.Name = "labelAverageHeartRate";
            this.labelAverageHeartRate.Size = new System.Drawing.Size(102, 13);
            this.labelAverageHeartRate.TabIndex = 92;
            this.labelAverageHeartRate.Text = "Average Heart Rate";
            // 
            // textBoxAverageHeartRate
            // 
            this.textBoxAverageHeartRate.Location = new System.Drawing.Point(237, 357);
            this.textBoxAverageHeartRate.Name = "textBoxAverageHeartRate";
            this.textBoxAverageHeartRate.ReadOnly = true;
            this.textBoxAverageHeartRate.Size = new System.Drawing.Size(100, 20);
            this.textBoxAverageHeartRate.TabIndex = 91;
            // 
            // textBoxAverageSpeed
            // 
            this.textBoxAverageSpeed.Location = new System.Drawing.Point(125, 357);
            this.textBoxAverageSpeed.Name = "textBoxAverageSpeed";
            this.textBoxAverageSpeed.ReadOnly = true;
            this.textBoxAverageSpeed.Size = new System.Drawing.Size(100, 20);
            this.textBoxAverageSpeed.TabIndex = 90;
            // 
            // labelAverageSpeed
            // 
            this.labelAverageSpeed.AutoSize = true;
            this.labelAverageSpeed.Location = new System.Drawing.Point(122, 341);
            this.labelAverageSpeed.Name = "labelAverageSpeed";
            this.labelAverageSpeed.Size = new System.Drawing.Size(81, 13);
            this.labelAverageSpeed.TabIndex = 89;
            this.labelAverageSpeed.Text = "Average Speed";
            // 
            // Weight
            // 
            this.Weight.AutoSize = true;
            this.Weight.Location = new System.Drawing.Point(414, 86);
            this.Weight.Name = "Weight";
            this.Weight.Size = new System.Drawing.Size(41, 13);
            this.Weight.TabIndex = 88;
            this.Weight.Text = "Weight";
            // 
            // labelVO2max
            // 
            this.labelVO2max.AutoSize = true;
            this.labelVO2max.Location = new System.Drawing.Point(368, 86);
            this.labelVO2max.Name = "labelVO2max";
            this.labelVO2max.Size = new System.Drawing.Size(47, 13);
            this.labelVO2max.TabIndex = 87;
            this.labelVO2max.Text = "VO2max";
            // 
            // textBoxWeight
            // 
            this.textBoxWeight.Location = new System.Drawing.Point(417, 102);
            this.textBoxWeight.Name = "textBoxWeight";
            this.textBoxWeight.ReadOnly = true;
            this.textBoxWeight.Size = new System.Drawing.Size(40, 20);
            this.textBoxWeight.TabIndex = 86;
            // 
            // textBoxVO2max
            // 
            this.textBoxVO2max.Location = new System.Drawing.Point(371, 102);
            this.textBoxVO2max.Name = "textBoxVO2max";
            this.textBoxVO2max.ReadOnly = true;
            this.textBoxVO2max.Size = new System.Drawing.Size(40, 20);
            this.textBoxVO2max.TabIndex = 85;
            // 
            // textBoxStartDelay
            // 
            this.textBoxStartDelay.Location = new System.Drawing.Point(125, 102);
            this.textBoxStartDelay.Name = "textBoxStartDelay";
            this.textBoxStartDelay.ReadOnly = true;
            this.textBoxStartDelay.Size = new System.Drawing.Size(100, 20);
            this.textBoxStartDelay.TabIndex = 84;
            // 
            // labelStartDelay
            // 
            this.labelStartDelay.AutoSize = true;
            this.labelStartDelay.Location = new System.Drawing.Point(122, 86);
            this.labelStartDelay.Name = "labelStartDelay";
            this.labelStartDelay.Size = new System.Drawing.Size(59, 13);
            this.labelStartDelay.TabIndex = 83;
            this.labelStartDelay.Text = "Start Delay";
            // 
            // labelRestHR
            // 
            this.labelRestHR.AutoSize = true;
            this.labelRestHR.Location = new System.Drawing.Point(322, 86);
            this.labelRestHR.Name = "labelRestHR";
            this.labelRestHR.Size = new System.Drawing.Size(48, 13);
            this.labelRestHR.TabIndex = 82;
            this.labelRestHR.Text = "Rest HR";
            // 
            // textBoxRestHR
            // 
            this.textBoxRestHR.Location = new System.Drawing.Point(325, 102);
            this.textBoxRestHR.Name = "textBoxRestHR";
            this.textBoxRestHR.ReadOnly = true;
            this.textBoxRestHR.Size = new System.Drawing.Size(40, 20);
            this.textBoxRestHR.TabIndex = 81;
            // 
            // textBoxMaxHR
            // 
            this.textBoxMaxHR.Location = new System.Drawing.Point(277, 102);
            this.textBoxMaxHR.Name = "textBoxMaxHR";
            this.textBoxMaxHR.ReadOnly = true;
            this.textBoxMaxHR.Size = new System.Drawing.Size(40, 20);
            this.textBoxMaxHR.TabIndex = 80;
            // 
            // labelMaxHR
            // 
            this.labelMaxHR.AutoSize = true;
            this.labelMaxHR.Location = new System.Drawing.Point(274, 86);
            this.labelMaxHR.Name = "labelMaxHR";
            this.labelMaxHR.Size = new System.Drawing.Size(46, 13);
            this.labelMaxHR.TabIndex = 79;
            this.labelMaxHR.Text = "Max HR";
            // 
            // labelSMode
            // 
            this.labelSMode.AutoSize = true;
            this.labelSMode.Location = new System.Drawing.Point(803, 20);
            this.labelSMode.Name = "labelSMode";
            this.labelSMode.Size = new System.Drawing.Size(41, 13);
            this.labelSMode.TabIndex = 78;
            this.labelSMode.Text = "SMode";
            // 
            // textBoxSMode
            // 
            this.textBoxSMode.Location = new System.Drawing.Point(806, 36);
            this.textBoxSMode.Name = "textBoxSMode";
            this.textBoxSMode.ReadOnly = true;
            this.textBoxSMode.Size = new System.Drawing.Size(100, 20);
            this.textBoxSMode.TabIndex = 77;
            // 
            // textBoxSessionLength
            // 
            this.textBoxSessionLength.Location = new System.Drawing.Point(125, 59);
            this.textBoxSessionLength.Name = "textBoxSessionLength";
            this.textBoxSessionLength.ReadOnly = true;
            this.textBoxSessionLength.Size = new System.Drawing.Size(100, 20);
            this.textBoxSessionLength.TabIndex = 76;
            // 
            // labelSessionLength
            // 
            this.labelSessionLength.AutoSize = true;
            this.labelSessionLength.Location = new System.Drawing.Point(122, 43);
            this.labelSessionLength.Name = "labelSessionLength";
            this.labelSessionLength.Size = new System.Drawing.Size(80, 13);
            this.labelSessionLength.TabIndex = 75;
            this.labelSessionLength.Text = "Session Length";
            // 
            // labelSessionDate
            // 
            this.labelSessionDate.AutoSize = true;
            this.labelSessionDate.Location = new System.Drawing.Point(10, 43);
            this.labelSessionDate.Name = "labelSessionDate";
            this.labelSessionDate.Size = new System.Drawing.Size(70, 13);
            this.labelSessionDate.TabIndex = 74;
            this.labelSessionDate.Text = "Session Date";
            // 
            // textBoxSessionDate
            // 
            this.textBoxSessionDate.Location = new System.Drawing.Point(13, 59);
            this.textBoxSessionDate.Name = "textBoxSessionDate";
            this.textBoxSessionDate.ReadOnly = true;
            this.textBoxSessionDate.Size = new System.Drawing.Size(100, 20);
            this.textBoxSessionDate.TabIndex = 73;
            // 
            // labelFileName
            // 
            this.labelFileName.AutoSize = true;
            this.labelFileName.Location = new System.Drawing.Point(94, 21);
            this.labelFileName.Name = "labelFileName";
            this.labelFileName.Size = new System.Drawing.Size(0, 13);
            this.labelFileName.TabIndex = 72;
            // 
            // buttonShowData
            // 
            this.buttonShowData.Location = new System.Drawing.Point(13, 16);
            this.buttonShowData.Name = "buttonShowData";
            this.buttonShowData.Size = new System.Drawing.Size(75, 23);
            this.buttonShowData.TabIndex = 71;
            this.buttonShowData.Text = "Show Data";
            this.buttonShowData.UseVisualStyleBackColor = true;
            this.buttonShowData.Click += new System.EventHandler(this.buttonShowData_Click);
            // 
            // labelInterval
            // 
            this.labelInterval.AutoSize = true;
            this.labelInterval.Location = new System.Drawing.Point(228, 86);
            this.labelInterval.Name = "labelInterval";
            this.labelInterval.Size = new System.Drawing.Size(42, 13);
            this.labelInterval.TabIndex = 70;
            this.labelInterval.Text = "Interval";
            // 
            // textBoxInterval
            // 
            this.textBoxInterval.Location = new System.Drawing.Point(231, 102);
            this.textBoxInterval.Name = "textBoxInterval";
            this.textBoxInterval.ReadOnly = true;
            this.textBoxInterval.Size = new System.Drawing.Size(40, 20);
            this.textBoxInterval.TabIndex = 69;
            // 
            // labelStartTime
            // 
            this.labelStartTime.AutoSize = true;
            this.labelStartTime.Location = new System.Drawing.Point(10, 86);
            this.labelStartTime.Name = "labelStartTime";
            this.labelStartTime.Size = new System.Drawing.Size(55, 13);
            this.labelStartTime.TabIndex = 68;
            this.labelStartTime.Text = "Start Time";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column9,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8});
            this.dataGridView1.Location = new System.Drawing.Point(13, 129);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(893, 150);
            this.dataGridView1.TabIndex = 67;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Row";
            this.Column9.Name = "Column9";
            this.Column9.Width = 50;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Heart";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Speed";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Cadence";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Altitude";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Power";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "PowerBalance";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Time";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Date";
            this.Column8.Name = "Column8";
            // 
            // textBoxStartTime
            // 
            this.textBoxStartTime.Location = new System.Drawing.Point(13, 102);
            this.textBoxStartTime.Multiline = true;
            this.textBoxStartTime.Name = "textBoxStartTime";
            this.textBoxStartTime.ReadOnly = true;
            this.textBoxStartTime.Size = new System.Drawing.Size(100, 20);
            this.textBoxStartTime.TabIndex = 66;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.Label labelSummaryDataDisplay;
        private System.Windows.Forms.Button buttonMetric;
        private System.Windows.Forms.Button buttonImperial;
        private System.Windows.Forms.Label labelSummaryDataValues;
        private System.Windows.Forms.Label labelTotalDistance;
        private System.Windows.Forms.TextBox textBoxTotalDistance;
        private System.Windows.Forms.Label labelMaximumSpeed;
        private System.Windows.Forms.TextBox textBoxMaximumSpeed;
        private System.Windows.Forms.TextBox textBoxMaximumAltitude;
        private System.Windows.Forms.Label labelMaximumAltitude;
        private System.Windows.Forms.TextBox textBoxAverageAltitude;
        private System.Windows.Forms.Label labelAverageAltitude;
        private System.Windows.Forms.Label labelMaximumPower;
        private System.Windows.Forms.TextBox textBoxMaximumPower;
        private System.Windows.Forms.Label labelAveragePower;
        private System.Windows.Forms.TextBox textBoxAveragePower;
        private System.Windows.Forms.TextBox textBoxMinimumHeartRate;
        private System.Windows.Forms.Label labelMinimumHeartRate;
        private System.Windows.Forms.TextBox textBoxMaximumHeartRate;
        private System.Windows.Forms.Label labelMaximumHeartRate;
        private System.Windows.Forms.Label labelAverageHeartRate;
        private System.Windows.Forms.TextBox textBoxAverageHeartRate;
        private System.Windows.Forms.TextBox textBoxAverageSpeed;
        private System.Windows.Forms.Label labelAverageSpeed;
        private System.Windows.Forms.Label Weight;
        private System.Windows.Forms.Label labelVO2max;
        private System.Windows.Forms.TextBox textBoxWeight;
        private System.Windows.Forms.TextBox textBoxVO2max;
        private System.Windows.Forms.TextBox textBoxStartDelay;
        private System.Windows.Forms.Label labelStartDelay;
        private System.Windows.Forms.Label labelRestHR;
        private System.Windows.Forms.TextBox textBoxRestHR;
        private System.Windows.Forms.TextBox textBoxMaxHR;
        private System.Windows.Forms.Label labelMaxHR;
        private System.Windows.Forms.Label labelSMode;
        private System.Windows.Forms.TextBox textBoxSMode;
        private System.Windows.Forms.TextBox textBoxSessionLength;
        private System.Windows.Forms.Label labelSessionLength;
        private System.Windows.Forms.Label labelSessionDate;
        private System.Windows.Forms.TextBox textBoxSessionDate;
        private System.Windows.Forms.Label labelFileName;
        private System.Windows.Forms.Button buttonShowData;
        private System.Windows.Forms.Label labelInterval;
        private System.Windows.Forms.TextBox textBoxInterval;
        private System.Windows.Forms.Label labelStartTime;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.TextBox textBoxStartTime;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxPower;
        private System.Windows.Forms.CheckBox checkBoxAltitude;
        private System.Windows.Forms.CheckBox checkBoxCadence;
        private System.Windows.Forms.CheckBox checkBoxSpeed;
        private System.Windows.Forms.CheckBox checkBoxHeartRate;
        private System.Windows.Forms.Button buttonDisplayGraph;
        private System.Windows.Forms.TextBox textBoxMouseUp;
        private System.Windows.Forms.TextBox textBoxMouseDown;
        private System.Windows.Forms.Label labelSelectionEnd;
        private System.Windows.Forms.Label labelSelectionStart;
        private System.Windows.Forms.Label labelFTP;
        private System.Windows.Forms.Label labelIF;
        private System.Windows.Forms.Label labelNP;
        private System.Windows.Forms.Label labelTSS;
        private System.Windows.Forms.TextBox textBoxIF;
        private System.Windows.Forms.TextBox textBoxNP;
        private System.Windows.Forms.TextBox textBoxTSS;
        private System.Windows.Forms.TextBox textBoxFTP;


    }
}

